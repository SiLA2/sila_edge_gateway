package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class ObservableCommandConfirmation {
    public static final String NAME = "ObservableCommandConfirmation";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>CommandExecutionUUID</Identifier> <DisplayName>Command Execution UUID</DisplayName> <Description>The unique identifier of the command.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Length>36</Length> <Pattern>[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}</Pattern> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>LifetimeOfExecution</Identifier> <DisplayName>Lifetime Of Execution</DisplayName> <Description>The Duration during which the Command Execution UUID is valid.</Description> <DataType> <DataTypeIdentifier>Duration</DataTypeIdentifier> </DataType> </Element> </Structure> </DataType>";
}
