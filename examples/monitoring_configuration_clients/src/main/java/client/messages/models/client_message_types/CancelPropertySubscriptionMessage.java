package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class CancelPropertySubscriptionMessage extends ClientMessage {
    private final String requestUUID;

    public CancelPropertySubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_CancelPropertySubscriptionMessage message) {
        this.requestUUID = message.getCancelPropertySubscriptionMessage().getValue();
    }

    @Override
    public String toString() {
        return "CancelPropertySubscriptionMessage{" +
                "\nRequestUUID : " + requestUUID +
                "\n}";
    }
}