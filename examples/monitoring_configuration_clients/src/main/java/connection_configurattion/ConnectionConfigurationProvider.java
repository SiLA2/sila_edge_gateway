package connection_configurattion;

import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceGrpc;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceOuterClass;
import sila_java.library.core.sila.errors.ExceptionGeneration;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAString;

@Slf4j
public class ConnectionConfigurationProvider {
    public static final String CLIENT_NAME_TO_CONNECT = "My Client";
    public static final String HOST = "localhost";
    public static final long PORT = 50308;

    private final ConnectionConfigurationServiceGrpc.ConnectionConfigurationServiceBlockingStub blockingStub;

    public ConnectionConfigurationProvider(final ManagedChannel channel) {
        this.blockingStub = ConnectionConfigurationServiceGrpc.newBlockingStub(channel);
    }

    public void connectUpstream() {
        ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters clientParameters = ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters.newBuilder()
                .setClientName(SiLAFramework.String.newBuilder().setValue(CLIENT_NAME_TO_CONNECT))
                .setSiLAClientHost(SiLAFramework.String.newBuilder().setValue(HOST))
                .setSiLAClientPort(SiLAFramework.Integer.newBuilder().setValue(PORT))
                .setPersist(SiLAFramework.Boolean.newBuilder().setValue(true))
                .build();

        try {
            ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Responses response = blockingStub.connectSiLAClient(clientParameters);
            log.info("ConnectionClientProvider: connectUpstream {}", response);
        } catch (StatusRuntimeException e) {
            log.error(ExceptionGeneration.generateMessage(e));
        }
    }

    public void getConfiguredClients() {
        ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Responses clients = blockingStub.getConfiguredSiLAClients(ConnectionConfigurationServiceOuterClass.Get_ConfiguredSiLAClients_Parameters.getDefaultInstance());
        log.info("ConnectionClientProvider: getConfiguredSiLAClients {}", clients);
    }

    public void disconnectClient() {
        ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters clientParameters = ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters.newBuilder()
                .setClientName(SiLAString.from(CLIENT_NAME_TO_CONNECT))
                .setRemove(SiLABoolean.from(true))
                .build();
        try {
            blockingStub.disconnectSiLAClient(clientParameters);
        } catch (StatusRuntimeException e) {
            log.error(ExceptionGeneration.generateMessage(e));
        }
    }

    public void enableServerInitiatedConnectionMode() {
        blockingStub.enableServerInitiatedConnectionMode(ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Parameters.getDefaultInstance());
    }

    public void disableServerInitiatedConnectionMode() {
        blockingStub.disableServerInitiatedConnectionMode(ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Parameters.getDefaultInstance());
    }
}
