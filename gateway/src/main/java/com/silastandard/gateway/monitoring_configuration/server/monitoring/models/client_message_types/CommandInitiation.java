package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class CommandInitiation {
    public static final String NAME = "CommandInitiation";
    public static final String TYPE = "<DataType><Structure><Element><Identifier>CommandIdentifier</Identifier><DisplayName>Command Identifier</DisplayName><Description>The fully qualified identifier of the observable command.</Description><DataType><Constrained><DataType><Basic>String</Basic></DataType><Constraints><FullyQualifiedIdentifier>CommandIdentifier</FullyQualifiedIdentifier></Constraints></Constrained></DataType></Element><Element><Identifier>CommandParameter</Identifier><DisplayName>Command Parameter</DisplayName><Description>The Parameter of the ObservableCommand. AnyType corresponds to the original CommandParameter DataType described in the feature.</Description><DataType><List><DataType><Basic>Any</Basic></DataType></List></DataType></Element></Structure></DataType>";

}
