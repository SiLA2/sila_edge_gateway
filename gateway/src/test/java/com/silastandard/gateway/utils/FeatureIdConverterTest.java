package com.silastandard.gateway.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FeatureIdConverterTest {
    private final String fullyQualifiedCallId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";

    @Test
    void getCallIDTest() {
        assertEquals("MakeCoffee", FeatureIdConverter.getCallID(fullyQualifiedCallId));
    }

    @Test
    void getFullyQualifiedFeatureIDTest() {
        assertEquals("ch.unitelabs/test/UnobservableCommandTest/v1", FeatureIdConverter.getFullyQualifiedFeatureID(fullyQualifiedCallId));
    }

    @Test
    void convertMetaDataIdTest() {
        String actual = "org.silastandard/core/SiLAService/v1/Metadata/AuthorizationToken";
        String expected = "sila-org.silastandard-core-silaservice-v1-metadata-authorizationtoken-bin";
        assertEquals(expected, FeatureIdConverter.convertMetaDataId(actual));
    }
}