package com.silastandard.gateway.monitoring_configuration.server.connection_configuration_client;

import lombok.Getter;

public class ConnectionException extends Exception {
    @Getter
    private final ExceptionStatus exceptionStatus;

    public ConnectionException(ExceptionStatus exceptionStatus, String message) {
        super(message);
        this.exceptionStatus = exceptionStatus;
    }

    public enum ExceptionStatus {
        CLIENT_OFFLINE,
        CLIENT_ALREADY_CONNECTED,
        CLIENT_NOT_CONNECTED
    }
}
