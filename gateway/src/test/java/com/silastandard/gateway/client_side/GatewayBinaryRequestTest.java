package com.silastandard.gateway.client_side;

import com.google.protobuf.ByteString;
import com.silastandard.gateway.client_side.message_services.*;
import com.silastandard.gateway.client_side.server.GatewayRequestHandler;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.client_side.binary_test_server.test_server.BinaryTestServer;
import com.silastandard.gateway.utils.BytesGenerator;
import junit.framework.Assert;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelperBuilder;
import client.messages.utils.ServerInfo;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static client.messages.utils.ScanUtilsForTest.scanAndWaitServerState;
import static client.messages.utils.ScanUtilsForTest.waitServerState;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GatewayBinaryRequestTest {
    private static final int TIMEOUT = 60;
    private UUID testServerUUID;

    private BinaryTestServer binaryTestServer;
    private final String clientName = "Test Client";
    final ServerInfo serverInfo = new ServerInfo(Paths.get("./config/testserver1.cfg"), 50100);
    private GatewayServices gatewayServices = new GatewayServices();

    private static final int BINARY_CHUNK_LENGTH = (int) Math.pow(2, 20); // 1048576 ~1mb

    @BeforeEach
    void setup() throws IOException {
        gatewayServices = new GatewayServices();
        binaryTestServer = new BinaryTestServer(
                ArgumentHelperBuilder.build(serverInfo.getPort(), "Test server", serverInfo.getConfigFile().getPath(), "local"));
        testServerUUID = serverInfo.getUUID(TIMEOUT);
        scanAndWaitServerState(gatewayServices.getServerManagerService(),testServerUUID, Server.Status.ONLINE, TIMEOUT);

    }

    @AfterEach
    void cleanup() throws TimeoutException, ExecutionException {
        log.info("Closing and cleaning up");
        binaryTestServer.close();
        waitServerState(gatewayServices.getServerManagerService(),testServerUUID, Server.Status.OFFLINE, TIMEOUT);
        gatewayServices.getServerManagerService().close();
    }

    @SneakyThrows
    @Test
    void createBinaryUploadRequestTest() {
        String fullyQualifiedCommandParameterId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee/Parameter/Sugar";

        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCreateBinaryUploadRequest(SiLACloudConnector.CreateBinaryUploadRequest.newBuilder()
                        .setCreateBinaryRequest(SiLABinaryTransfer.CreateBinaryRequest.newBuilder()
                                .setBinarySize(200)
                                .setChunkCount(20)
                                .setParameterIdentifier(fullyQualifiedCommandParameterId)
                                .build())
                        .build()
                );

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);

        SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse = silaServerMessageResult.getCreateBinaryResponse();

        Assert.assertEquals(createBinaryResponse.getLifetimeOfBinary().getSeconds(), 600);
    }

    @SneakyThrows
    @Test
    void uploadChunkResponseTest() {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        final int nbChunks = 6;
        final int chunkLength = BINARY_CHUNK_LENGTH;
        final byte[] randomBytes = BytesGenerator.getRandomBytes(chunkLength);
        final long nbBytes = (long) nbChunks * chunkLength;

        String fullyQualifiedCommandParameterId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee/Parameter/Sugar";

        SiLACloudConnector.SILAClientMessage clientMessageCreateBinary = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCreateBinaryUploadRequest(SiLACloudConnector.CreateBinaryUploadRequest.newBuilder()
                        .setCreateBinaryRequest(SiLABinaryTransfer.CreateBinaryRequest.newBuilder()
                                .setBinarySize(nbBytes)
                                .setChunkCount(nbChunks)
                                .setParameterIdentifier(fullyQualifiedCommandParameterId)
                                .build())
                        .build())
                .build();

        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
        gatewayRequestHandler.handleRequest(clientMessageCreateBinary);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse = silaServerMessageResult.getCreateBinaryResponse();
        String binaryTransferUUID = createBinaryResponse.getBinaryTransferUUID();


        result = new ArrayList<>();
        latch = new CountDownLatch(nbChunks);
        for (int i = 0; i < nbChunks; ++i) {
            SiLACloudConnector.SILAClientMessage clientMessageUploadChunk = SiLACloudConnector.SILAClientMessage.newBuilder()
                    .setUploadChunkRequest(SiLABinaryTransfer.UploadChunkRequest.newBuilder()
                            .setBinaryTransferUUID(binaryTransferUUID)
                            .setChunkIndex(i)
                            .setPayload(ByteString.copyFrom(randomBytes))
                            .build())
                    .build();

            gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);
            gatewayRequestHandler.handleRequest(clientMessageUploadChunk);
        }
        assertTrue(latch.await(10, TimeUnit.SECONDS));

        for (int i = 0; i < nbChunks; ++i) {
            SiLACloudConnector.SILAServerMessage silaServerMessageResultUploadChunk = result.get(i);
            SiLABinaryTransfer.UploadChunkResponse uploadChunkResponse = silaServerMessageResultUploadChunk.getUploadChunkResponse();
            assertEquals(i, uploadChunkResponse.getChunkIndex());
            assertEquals(binaryTransferUUID, uploadChunkResponse.getBinaryTransferUUID());
        }

        // Delete Binary -> Error because Binary is removed on Test Server after upload completion (UploadService.java)
        result = new ArrayList<>();
        latch = new CountDownLatch(1);
        gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        SiLACloudConnector.SILAClientMessage clientMessageDeleteBinary = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setDeleteUploadedBinaryRequest(SiLABinaryTransfer.DeleteBinaryRequest.newBuilder()
                        .setBinaryTransferUUID(binaryTransferUUID)
                        .build())
                .build();

        gatewayRequestHandler.handleRequest(clientMessageDeleteBinary);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        silaServerMessageResult = result.get(0);

        assertTrue(silaServerMessageResult.hasBinaryTransferError());
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = silaServerMessageResult.getBinaryTransferError();
        assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, binaryTransferError.getErrorType());
    }

    @SneakyThrows
    @Test
    void uploadChunkResponseInvalidUUIDest() {
        final byte[] randomBytes = BytesGenerator.getRandomBytes(BINARY_CHUNK_LENGTH);

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setUploadChunkRequest(SiLABinaryTransfer.UploadChunkRequest.newBuilder()
                        .setBinaryTransferUUID("invalidUUID")
                        .setChunkIndex(0)
                        .setPayload(ByteString.copyFrom(randomBytes))
                        .build())
                .build();

        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        gatewayRequestHandler.handleRequest(clientMessage);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);

        assertTrue(silaServerMessageResult.hasBinaryTransferError());
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = silaServerMessageResult.getBinaryTransferError();
        assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, binaryTransferError.getErrorType());

    }

    @SneakyThrows
    @Test
    void deleteBinaryRequestTest() {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);

        String fullyQualifiedCommandParameterId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee/Parameter/Sugar";

        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setCreateBinaryUploadRequest(SiLACloudConnector.CreateBinaryUploadRequest.newBuilder()
                        .setCreateBinaryRequest(SiLABinaryTransfer.CreateBinaryRequest.newBuilder()
                                .setBinarySize(200)
                                .setChunkCount(20)
                                .setParameterIdentifier(fullyQualifiedCommandParameterId)
                                .build())
                        .build()
                );

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));
        final String binaryTransferUUID = result.get(0).getCreateBinaryResponse().getBinaryTransferUUID();

        result = new ArrayList<>();
        latch = new CountDownLatch(1);
        gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        SiLACloudConnector.SILAClientMessage clientMessageDeleteBinary = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setDeleteUploadedBinaryRequest(SiLABinaryTransfer.DeleteBinaryRequest.newBuilder()
                        .setBinaryTransferUUID(binaryTransferUUID)
                        .build())
                .build();

        gatewayRequestHandler.handleRequest(clientMessageDeleteBinary);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);

        assertTrue(silaServerMessageResult.hasDeleteBinaryResponse());
        SiLABinaryTransfer.DeleteBinaryResponse deleteBinaryResponse = silaServerMessageResult.getDeleteBinaryResponse();
        assertEquals(SiLABinaryTransfer.DeleteBinaryResponse.getDefaultInstance(), deleteBinaryResponse);
    }

    @SneakyThrows
    @Test
    void deleteBinaryRequestInvalidUUIDTest() {
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setDeleteUploadedBinaryRequest(SiLABinaryTransfer.DeleteBinaryRequest.newBuilder()
                        .setBinaryTransferUUID(UUID.randomUUID().toString())
                        .build())
                .build();

        gatewayRequestHandler.handleRequest(clientMessage);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);

        assertTrue(silaServerMessageResult.hasBinaryTransferError());
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = silaServerMessageResult.getBinaryTransferError();
        assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, binaryTransferError.getErrorType());
    }

    @SneakyThrows
    @Test
    void getBinaryInfoRequestTest() {
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setGetBinaryInfoRequest(SiLABinaryTransfer.GetBinaryInfoRequest.newBuilder()
                        .setBinaryTransferUUID(UUID.randomUUID().toString())
                        .build())
                .build();

        gatewayRequestHandler.handleRequest(clientMessage);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        assertTrue(silaServerMessageResult.hasGetBinaryResponse());
        SiLABinaryTransfer.GetBinaryInfoResponse binaryInfoResponse = silaServerMessageResult.getGetBinaryResponse();
        assertEquals(100, binaryInfoResponse.getBinarySize());
    }

    @SneakyThrows
    @Test
    void getBinaryInfoRequestInvalidUUIDTest() {
        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result);

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setGetBinaryInfoRequest(SiLABinaryTransfer.GetBinaryInfoRequest.newBuilder()
                        .setBinaryTransferUUID("invalidUUID")
                        .build())
                .build();

        gatewayRequestHandler.handleRequest(clientMessage);
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);

        assertTrue(silaServerMessageResult.hasBinaryTransferError());
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = silaServerMessageResult.getBinaryTransferError();
        assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, binaryTransferError.getErrorType());
    }

    @SneakyThrows
    @Test
    void getChunkRequestTest() {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);


        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setGetChunkRequest(SiLABinaryTransfer.GetChunkRequest.newBuilder()
                        .setBinaryTransferUUID(UUID.randomUUID().toString())
                        .setLength(2)
                        .setOffset(0)
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(5, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        assertTrue(silaServerMessageResult.hasGetChunkResponse());

        // produce Binary Download Failed with length = 0
        result = new ArrayList<>();
        latch = new CountDownLatch(1);
        gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setGetChunkRequest(SiLABinaryTransfer.GetChunkRequest.newBuilder()
                        .setBinaryTransferUUID(UUID.randomUUID().toString())
                        .setLength(0)
                        .setOffset(0)
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(5, TimeUnit.SECONDS));

        silaServerMessageResult = result.get(0);
        assertTrue(silaServerMessageResult.hasBinaryTransferError());
        assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_DOWNLOAD_FAILED, silaServerMessageResult.getBinaryTransferError().getErrorType());
    }

    @SneakyThrows
    @Test
    void getChunkRequestInvalidUUIDTest() {
        CommandExecutionService commandExecutionService = new CommandExecutionService(testServerUUID, clientName, gatewayServices);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(testServerUUID, clientName, gatewayServices);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(testServerUUID, clientName, gatewayServices);
        BinaryTransferService binaryTransferService = new BinaryTransferService(testServerUUID, clientName, gatewayServices);


        List<SiLACloudConnector.SILAServerMessage> result = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        GatewayRequestHandler gatewayRequestHandler = getRequestHandler(latch, result, commandExecutionService, unobservablePropertyService, observableCommandExecutionService, observablePropertyService, binaryTransferService);

        SiLACloudConnector.SILAClientMessage.Builder clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setGetChunkRequest(SiLABinaryTransfer.GetChunkRequest.newBuilder()
                        .setBinaryTransferUUID("invalidUUID")
                        .setLength(100)
                        .setOffset(10)
                        .build());

        gatewayRequestHandler.handleRequest(clientMessage.build());
        assertTrue(latch.await(1, TimeUnit.SECONDS));

        SiLACloudConnector.SILAServerMessage silaServerMessageResult = result.get(0);
        assertTrue(silaServerMessageResult.hasBinaryTransferError());
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = silaServerMessageResult.getBinaryTransferError();
        assertEquals(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, binaryTransferError.getErrorType());
    }

    public GatewayRequestHandler getRequestHandler(CountDownLatch latch,
                                                   List<SiLACloudConnector.SILAServerMessage> result) {
        return new GatewayRequestHandler(GatewayRequestHandlerTest.getMessageStreamObserver(latch, result),
                new CommandExecutionService(testServerUUID, clientName, gatewayServices),
                new UnobservablePropertyService(testServerUUID, clientName, gatewayServices),
                new ObservablePropertyService(testServerUUID, clientName, gatewayServices),
                new ObservableCommandExecutionService(testServerUUID, clientName, gatewayServices),
                new BinaryTransferService(testServerUUID, clientName, gatewayServices));
    }

    private GatewayRequestHandler getRequestHandler(CountDownLatch latch,
                                                    List<SiLACloudConnector.SILAServerMessage> result,
                                                    CommandExecutionService commandExecutionService,
                                                    UnobservablePropertyService unobservablePropertyService,
                                                    ObservableCommandExecutionService observableCommandExecutionService,
                                                    ObservablePropertyService observablePropertyService,
                                                    BinaryTransferService binaryTransferService) {
        return new GatewayRequestHandler(
                GatewayRequestHandlerTest.getMessageStreamObserver(latch, result),
                commandExecutionService,
                unobservablePropertyService,
                observablePropertyService,
                observableCommandExecutionService,
                binaryTransferService);
    }
}
