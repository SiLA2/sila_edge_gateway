## Test implementations to test the gateway
These test implementations contain three SiLA<1.0> servers and three different server-initiated clients for different testing purposes.
The executable jars can be launched from the root directory of the project.

&nbsp;

#### Test servers
Three SiLA<1.0> servers to test the gateway with the following server-initiated clients.
- `java -jar examples/test_implementations/target/CoverControllerServer-exec.jar -n local`
- `java -jar examples/test_implementations/target/HelloSiLAServer-exec.jar -n local`
- `java -jar examples/test_implementations/target/TimerServer-exec.jar -n local`

---

#### Multiple Server Client
A server-initiated client that communicates with the three test servers with randomly repeated calls.

Start the MultipleServerClient:
- `java -jar examples/test_implementations/target/TestMultipleServerClient-exec.jar`

---

#### Load Test Client
A server-initiated client that issues repeated calls to the gateway after opening the streams for the test servers.
It records how many RPCs have been completed after a certain time.

Start the LoadTestClient:
- `java -jar examples/test_implementations/target/LoadTestClient-exec.jar`

---

#### Log server information
This is an implementation for a server-initiated client which uses the SiLA Service.
The goal is to show that the gateway can handle the used message types specified in the SiLA Service.

Start the log server information client:
- `java -jar examples/test_implementations/target/LogServerInformationClient-exec.jar`

