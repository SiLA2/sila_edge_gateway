package client.hellosilaserver_impl;

import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.core.sila.types.SiLAString;

import java.util.function.Consumer;

public class HelloSiLAClient {
    private final CloudConnectedServer server;

    public HelloSiLAClient(CloudConnectedServer server) {
        this.server = server;
    }

    public void getYear(Consumer<String> resultCallback) {
        YearProperty yearProperty = YearProperty.builder()
                .resultCallback(r -> resultCallback.accept(r.getStartYear().toString()))
                .build();
        server.getUnobservablePropertyService().readProperty(yearProperty);
    }

    public void greeting(String parameterName, Consumer<String> resultCallback) {
        UnobservableGreetingCommand unobservableGreetingCommand = UnobservableGreetingCommand.builder()
                .parameters(GreetingProviderOuterClass.SayHello_Parameters.newBuilder().setName(SiLAString.from(parameterName)).build())
                .resultCallback(r -> resultCallback.accept(r.getGreeting().toString()))
                .build();
        server.getCommandExecutionService().executeCommand(unobservableGreetingCommand);
    }
}
