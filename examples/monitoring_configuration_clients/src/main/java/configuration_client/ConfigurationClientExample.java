package configuration_client;

import io.grpc.ManagedChannel;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.clients.ChannelFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A client to test the GatewayConfigurationService Feature.
 */
public class ConfigurationClientExample {
    private static final String GATEWAY_HOST = "localhost";
    private static final int GATEWAY_PORT = 50400;

    public static void main(String[] args) throws InterruptedException {

        final ManagedChannel managedChannel = ChannelFactory.withEncryption(GATEWAY_HOST, GATEWAY_PORT);
        try {

            ConfigurationProvider serverConfigurationClient = new ConfigurationProvider(managedChannel);
            testFeature(serverConfigurationClient);

        } finally {
            managedChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    }

    private static void testFeature(ConfigurationProvider serverConfigurationClient) {
        // Trigger Server Discovery
        serverConfigurationClient.triggerServerDiscovery();

        // Get servers
        List<SiLAFramework.String> servers = serverConfigurationClient.getServers();

        servers.forEach(serverUUID -> {
                System.out.println("-----------\n");
                serverConfigurationClient.getServerInformation(serverUUID);
                serverConfigurationClient.getImplementedFeature(serverUUID);
            }
        );
    }
}
