package client.messages.models.client_message_types;

import client.messages.ClientRequestMessage;
import com.google.gson.Gson;
import lombok.Getter;

@Getter
public abstract class ClientMessage implements ClientRequestMessage.ClientRequestMessageTypes{

    @Override
    public String toJson() { return new Gson().toJson(this); }
}
