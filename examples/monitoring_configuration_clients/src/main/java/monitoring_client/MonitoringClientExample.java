package monitoring_client;

import client.messages.ClientRequestMessage;
import client.messages.MessageParserService;
import client.messages.ServerResponseMessage;
import configuration_client.ConfigurationProvider;
import io.grpc.Context;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.clients.ChannelFactory;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A client to test the GatewayMonitoringService Feature. This client just monitors the messages going through the gateway.
 */
@Slf4j
public class MonitoringClientExample {
    private static final String GATEWAY_HOST = "localhost";
    private static final int GATEWAY_PORT = 50400;

    public static void main(String[] args) throws InterruptedException {
        final ManagedChannel managedChannel = ChannelFactory.withEncryption(GATEWAY_HOST, GATEWAY_PORT);
        try {
            // Trigger server discovery
            ConfigurationProvider configurationProvider = new ConfigurationProvider(managedChannel);
            configurationProvider.triggerServerDiscovery();

            // Get first Servers UUID
            List<SiLAFramework.String> serverUUIDs = configurationProvider.getServers();
            if(serverUUIDs.isEmpty()) {
                System.out.println("No Server found in network!");
                return;
            }

            // Test GatewayMonitoringService feature with the found serverUUIDs
            MonitoringProvider monitoringProvider = new MonitoringProvider(managedChannel);
            monitorMessageInGateway(serverUUIDs, monitoringProvider);

        } finally {
            managedChannel.shutdown().awaitTermination(10, TimeUnit.SECONDS);
        }
    }

    public static void monitorMessageInGateway(List<SiLAFramework.String> serverUUIDs, MonitoringProvider monitoringProvider) {
        final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        final long monitoring_duration_sec = 30;
        final MessageParserService messageParserService = new MessageParserService();
        final Context.CancellableContext cancellableContext = Context.current().withCancellation().withDeadlineAfter(monitoring_duration_sec, TimeUnit.SECONDS, scheduledExecutorService);

        cancellableContext.run(() -> {
            try {
                SiLAFramework.CommandConfirmation commandConfirmation = monitoringProvider.observerCurrentMessagesCommand(serverUUIDs); //Command to observe current messages
                SiLAFramework.CommandExecutionUUID commandExecutionUUID = commandConfirmation.getCommandExecutionUUID();
                Iterator<SiLAFramework.ExecutionInfo> executionInfoIterator = monitoringProvider.currentMessageInfo(commandExecutionUUID);

                Iterator<GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses> currentMessagesIntermediate
                        = monitoringProvider.observeCurrentMessagesIntermediate(commandExecutionUUID);
                log.info("Observe current Messages Intermediate: {}", commandExecutionUUID);

                SiLAFramework.ExecutionInfo executionInfo = executionInfoIterator.next();
                log.info("ExecutionInfo CommandStatus: {}", executionInfo.getCommandStatus());
                log.info("Started monitoring Gateway messages: [for " + monitoring_duration_sec + " sec.]");

                while (executionInfo.getCommandStatus().equals(SiLAFramework.ExecutionInfo.CommandStatus.running)) {
                    GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses message = currentMessagesIntermediate.next();
                    if (message.hasClientRequestMessage()) {
                        ClientRequestMessage requestMessage = messageParserService.parseSiLAClientMessage(message.getClientRequestMessage());
                        System.out.println(requestMessage);

                    } else if (message.hasServerResponseMessage()) {
                        ServerResponseMessage serverResponseMessage = messageParserService.parseSiLAServerMessage(message.getServerResponseMessage());

                        System.out.println(serverResponseMessage);
                    }
                }
            } catch (StatusRuntimeException e) {
                log.info("Monitoring deadline exceeded.");
            }
        });
        cancellableContext.close();
        scheduledExecutorService.shutdown();
        log.info("Stopped monitoring.");
    }
}

