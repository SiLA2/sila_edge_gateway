package client.hellosilaserver_impl;

import com.google.protobuf.ByteString;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.library.cloud.client.property.UnobservablePropertyImpl;

import java.util.function.Consumer;

import static sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.Get_StartYear_Responses.parser;

@Slf4j
@Getter
public class YearProperty extends UnobservablePropertyImpl<GreetingProviderOuterClass.Get_StartYear_Parameters, GreetingProviderOuterClass.Get_StartYear_Responses> {
    private static final String PROPERTY_ID = "org.silastandard/examples/GreetingProvider/v1/Property/StartYear";

    @Builder
    public YearProperty(GreetingProviderOuterClass.Get_StartYear_Parameters parameters, Consumer<GreetingProviderOuterClass.Get_StartYear_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, parser(), errorCallback);
    }

    @Override
    public void onResponse(GreetingProviderOuterClass.Get_StartYear_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError silaError) {
        log.warn("error received: {}", silaError);
    }

    @Override
    public String getPropertyId() {
        return PROPERTY_ID;
    }

    @Override
    public ByteString getPayload() {
        GreetingProviderOuterClass.Get_StartYear_Responses yearResponses = GreetingProviderOuterClass.Get_StartYear_Responses.newBuilder()
                .setStartYear(SiLAFramework.Integer.newBuilder().setValue(2020))
                .build();
        return yearResponses.toByteString();
    }

}
