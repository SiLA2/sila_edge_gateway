package client.messages;

import com.google.protobuf.DynamicMessage;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types.CommandExecution;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types.CommandGetResponse;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types.PropertyRead;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types.CommandResponse;
import com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types.ErrorResponse;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.types.SiLAAny;
import sila_java.library.core.sila.types.SiLABoolean;
import sila_java.library.core.sila.types.SiLAString;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageParserServiceTest {
    MessageParserService messageParserService = new MessageParserService();

    @Test
    void PropertyReadMessageTest() {
        String fullyQualifiedPropertyId = "ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString";

        final SiLAFramework.Any anyMessage = SiLAAny.from(PropertyRead.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_PropertyReadMessage
                        .newBuilder()
                        .setPropertyReadMessage(SiLAString.from(fullyQualifiedPropertyId))
                        .build());


        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct.newBuilder()
                        .setSiLAClient(SiLAString.from("Client"))
                        .setSiLAServer(SiLAString.from("Server"))
                        .setMessageType(SiLAString.from(PropertyRead.NAME))
                        .setMessage(anyMessage)
                        .build();

        ClientRequestMessage clientRequestMessage = messageParserService.parseSiLAClientMessage(message);
        String expected = "PropertyReadMessage{\n" +
                "Property Identifier : ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString\n" +
                "}";
        assertEquals(expected, clientRequestMessage.getMessage().toString());
    }

    @SneakyThrows
    @Test
    void commandExecutionMessageTest() {
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";
        UnobservableCommandTestOuterClass.MakeCoffee_Parameters parameters = UnobservableCommandTestOuterClass.MakeCoffee_Parameters.newBuilder()
                .setSugar(SiLABoolean.from(true)).build();
        DynamicMessage requestMessage = DynamicMessage.parseFrom(parameters.getDescriptorForType(), parameters.getSugar().toByteString());
        SiLAFramework.Any anyParameter = SiLAAny.from("<DataType> <Basic>Boolean</Basic> </DataType>", requestMessage);

        final SiLAFramework.Any anyMessage = SiLAAny.from(CommandExecution.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage
                        .newBuilder()
                        .setCommandExecutionMessage(GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.newBuilder()
                                .setCommandIdentifier(SiLAString.from(fullyQualifiedCommandId))
                                .addCommandParameters(GatewayMonitoringServiceOuterClass.DataType_CommandExecutionMessage.CommandExecutionMessage_Struct.CommandParameters_Struct.newBuilder()
                                        .setName(SiLAString.from("Sugar"))
                                        .setValue(anyParameter)
                                        .build())
                                .build())
                        .build());

        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct.newBuilder()
                        .setSiLAClient(SiLAString.from("Client"))
                        .setSiLAServer(SiLAString.from("Server"))
                        .setMessageType(SiLAString.from(CommandExecution.NAME))
                        .setMessage(anyMessage)
                        .build();

        ClientRequestMessage clientRequestMessage = messageParserService.parseSiLAClientMessage(message);

        final String expected = "CommandExecutionMessage{\n" +
                "Command Identifier : ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee\n" +
                "Command Parameter :\n" +
                " {Sugar={\"value\":true}}\n" +
                "}";
        assertEquals(expected, clientRequestMessage.getMessage().toString());
    }

    @Test
    void CommandGetResponseMessageTest() {
        UUID commandExecutionUUID = UUID.randomUUID();
        String fullyQualifiedCommandId = "ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee";

        final SiLAFramework.Any anyMessage = SiLAAny.from(PropertyRead.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage
                        .newBuilder()
                        .setCommandGetResponseMessage(GatewayMonitoringServiceOuterClass.DataType_CommandGetResponseMessage.CommandGetResponseMessage_Struct.newBuilder()
                                .setCommandIdentifier(SiLAString.from(fullyQualifiedCommandId))
                                .setCommandExecutionUUID(SiLAString.from(commandExecutionUUID.toString()))
                                .build())
                        .build());

        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct message =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ClientRequestMessage_Struct.newBuilder()
                        .setSiLAClient(SiLAString.from("Client"))
                        .setSiLAServer(SiLAString.from("Server"))
                        .setMessageType(SiLAString.from(CommandGetResponse.NAME))
                        .setMessage(anyMessage)
                        .build();

        ClientRequestMessage clientRequestMessage = messageParserService.parseSiLAClientMessage(message);

        String expected = "CommandGetResponseMessage{\n" +
                "CommandIdentifier : ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee\n" +
                "CommandExecutionUUID : "+commandExecutionUUID.toString()+"\n" +
                "}";
        assertEquals(expected, clientRequestMessage.getMessage().toString());
    }

    @SneakyThrows
    @Test
    void CommandResponseMessageTest() {
        UnobservableCommandTestOuterClass.MakeCoffee_Responses responses = UnobservableCommandTestOuterClass.MakeCoffee_Responses.newBuilder()
                .setResult(SiLAString.from("Coffee is ready!"))
                .build();

        DynamicMessage resultMessage = DynamicMessage.parseFrom(responses.getResult().getDescriptorForType(), responses.getResult().toByteString());
        SiLAFramework.Any anyTypeResponse= SiLAAny.from("<DataType><Basic>String</Basic></DataType>", resultMessage);

        final SiLAFramework.Any anyMessage = SiLAAny.from(CommandResponse.TYPE,
                GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage
                        .newBuilder()
                        .setCommandResponseMessage(GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.CommandResponseMessage_Struct.newBuilder()
                                .addCommandResponse(GatewayMonitoringServiceOuterClass.DataType_CommandResponseMessage.CommandResponseMessage_Struct.CommandResponse_Struct.newBuilder()
                                        .setIdentifier(SiLAString.from("Result"))
                                        .setValue(anyTypeResponse)
                                .build())
                        .build())
                .build());

        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct message =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct.newBuilder()
                        .setSiLAClient(SiLAString.from("Client"))
                        .setSiLAServer(SiLAString.from("Server"))
                        .setMessageType(SiLAString.from(CommandResponse.NAME))
                        .setMessage(anyMessage)
                        .build();

        ServerResponseMessage serverResponseMessage = messageParserService.parseSiLAServerMessage(message);

        String expected = "CommandResponseMessage{\n" +
                "Command Identifier: \n" +
                " Command Response:\n" +
                "{Result={\"value\":\"Coffee is ready!\"}}\n" +
                "}";

        assertEquals(expected,serverResponseMessage.getMessage().toString());
    }

    @Test
    void errorMessageTest() {
        GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct.Builder message =
                GatewayMonitoringServiceOuterClass.ObserveCurrentMessages_IntermediateResponses.ServerResponseMessage_Struct.newBuilder()
                        .setSiLAClient(SiLAString.from("Client"))
                        .setSiLAServer(SiLAString.from("Server"));

        message.setMessageType(SiLAString.from(ErrorResponse.DEFINED_ERROR_NAME));
        message.setMessage(SiLAAny.from(ErrorResponse.DEFINED_ERROR_TYPE,
                GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage
                        .newBuilder()
                        .setDefinedExecutionErrorMessage(GatewayMonitoringServiceOuterClass.DataType_DefinedExecutionErrorMessage.DefinedExecutionErrorMessage_Struct.newBuilder()
                                .setErrorIdentifier(SiLAString.from("example error identifier"))
                                .setMessage(SiLAString.from("example error message"))
                                .build())
                        .build()));

        ServerResponseMessage serverResponseMessage = messageParserService.parseSiLAServerMessage(message.build());

        String expected = "DefinedExecutionErrorMessage{\n" +
                "Error Identifier : example error identifier\n" +
                "Error Message : example error message\n" +
                "}";
        assertEquals(expected, serverResponseMessage.getMessage().toString());


        message.setMessageType(SiLAString.from(ErrorResponse.UNDEFINED_ERROR_NAME));
        message.setMessage(SiLAAny.from("<DataType> <Basic>String</Basic> </DataType>",
                GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage
                        .newBuilder()
                        .setUndefinedExecutionErrorMessage(SiLAString.from("Undefined Error Message"))
                        .build()));
        serverResponseMessage = messageParserService.parseSiLAServerMessage(message.build());
        expected = "UndefinedExecutionErrorMessage{\n" +
                "Error Message : Undefined Error Message\n" +
                "}";
        assertEquals(expected, serverResponseMessage.getMessage().toString());


        message.setMessageType(SiLAString.from(ErrorResponse.CONNECTION_ERROR_NAME));
        message.setMessage(SiLAAny.from(ErrorResponse.CONNECTION_ERROR_TYPE,
                GatewayMonitoringServiceOuterClass.DataType_ConnectionErrorMessage
                        .newBuilder()
                        .setConnectionErrorMessage(SiLAString.from("Connection Error Message"))
                        .build()));

        serverResponseMessage = messageParserService.parseSiLAServerMessage(message.build());
        expected = "ConnectionErrorMessage{\n" +
                "Message : Connection Error Message\n" +
                "}";
        assertEquals(expected, serverResponseMessage.getMessage().toString());
    }
}