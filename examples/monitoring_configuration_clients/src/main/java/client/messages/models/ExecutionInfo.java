package client.messages.models;

import lombok.Builder;

@Builder
public class ExecutionInfo {
    private final String commandExecutionStatus;
    private final Double progressInfo;
    private final Duration estimatedRemainingTime;
    private final Duration updatedLifetimeOfExecution;
}
