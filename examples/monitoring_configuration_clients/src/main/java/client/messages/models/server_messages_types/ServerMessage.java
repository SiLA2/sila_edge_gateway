package client.messages.models.server_messages_types;

import client.messages.ServerResponseMessage;
import com.google.gson.Gson;
import lombok.Getter;

@Getter
public abstract class ServerMessage implements ServerResponseMessage.ServerResponseMessageTypes {
    @Override
    public String toJson() { return new Gson().toJson(this); }
}
