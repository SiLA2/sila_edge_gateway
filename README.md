> :warning: **This project is deprecated. It uses an outdated version of the SiLA2 standard. Please use https://gitlab.com/SiLA2/tools/sila_edge_gateway instead.** :warning:

# SiLA Gateway

### About The Project
In Version 1.0.
The SiLA edge gateway is a middleware between SiLA Servers in the laboratory and SiLA Clients in the cloud.

#### What does the SiLA edge gateway do?
##### Connect multiple SiLA servers to one SiLA client over same path

A server-initiated client can address multiple SiLA servers across networks through the gateway. All SILA servers connected to the edge gateway are routed to the same SILA client endpoint.

##### Cloud-enable SiLA2 1.0 servers without change of code

Existing SiLA2 1.0 servers that were not previously cloud-enabled can be made cloud-enabled via the gateway. 

##### Server-initiated connection establishment of the gateway with a connection (avoids firewall problems):

The gateway establishes the connection to a SiLA client externally. Only one connection (one port) is needed. This brings all the advantages of a server-initiated connection. (read more about server-initiated connections in the SILA2 specification)

##### Configuration: define which servers are connected to the client side 

Configuration functions and information from connected SiLA servers and SiLA clients can be accessed via the gateway.

These include:

+ Searching for SiLA servers on the network (Server Discovery). Found SiLA servers are added to the gateway. Only servers that have Server Discovery enabled can be found. 

+ Manually adding the servers to the gateway by specifying port and host. This will make the features of the server available to the gateway connected client. This also applies to the servers found by Server Discovery.

+ Remove the servers from the gateway again. The server with its features can thus no longer be seen and used by clients connected to the gateway.

+ Information such as server name, description, server status, supported features, etc. can be retrieved from a SiLA server known to the gateway. A monitoring & configuration client is provided for this purpose.

+ Using the ConnectionConfigurationService(SiLA standard):

+ The gateway can be connected or disconnected to a SiLA client. Optionally, the connection details can be kept persistent, so that the gateway restores the connection after a reboot. 


##### Message monitoring: trace all messages going through gateway

Monitor and retrieve the current messages flowing between SiLA servers and the client, through the gateway. This data can also be further processed, stored, logged as an audit trail, used for traffic predictions and analysis.

### Requirements

- Java 11 Runtime/SDK or greater
- Maven (3.5+)
- Git 

### Project Progress

Current Work:
- Minor bug fixes

### Issues
- The MonitoringMessageProvider can't handle (all) complex datatypes in parameters and responses(from Anytype)

### Installation

1. Clone repository:

   With SSH: `git clone https://gitlab.com/SiLA2/sila_edge_gateway.git`

   With HTTPS: `git clone git@gitlab.com:SiLA2/sila_edge_gateway.git`

2. Clone submodule (sila_java): 
    
   `git submodule update --init --recursive`

3. Build project:

   `mvn clean install -DskipTests`

### Start Gateway
Launch executable .jar from the root directory of the project on a specific port.

`java -jar gateway/target/gateway-exec.jar -p <port>`

### Example Usage

#### With LogServerInformationClient:
1. Start a SiLA Server with Server Discovery on the same network where the Gateway gets started:

    Example servers can be found in examples/test_implementations. Use [README](examples/README.md) for more information.

2. Start the Gateway on Port 50400: (The test client tries to connect to Port 50400)

    `java -jar gateway/target/gateway-exec.jar -p 50400`

3. Start the Text-User-Interface Client [TUITestClient](examples/monitoring_configuration_clients/src/main/java/TUITestClient.java):

    `java -jar examples/monitoring_configuration_clients/target/TUITestClient-exec.jar`
    
4.  Start the Log server Information Client [TesLogServerInformationClient](examples/test_implementations/src/main/java/client/LogServerInformationClient.java):

    When connected to Gateway, it logs the server information from all to the gateway connected servers.
    
    `java -jar examples/test_implementations/target/LogServerInformationClient-exec.jar`
    
5. Use the TUITestClient to do some example configuration and connection:

    (TUITestClient is incomplete in terms of possible commands and properties of the features)
    
#### With LogServerInformationClient: (Monitoring example)
1. Start SiLA Test servers:

    Use [README](examples/test_implementations/README.md) for more information on how to start the test servers. Start at least one of the test servers.

2. Start the Gateway on Port 50400: (The test client tries to connect to Port 50400)

    `java -jar gateway/target/gateway-exec.jar -p 50400`

3. Start the Text-User-Interface Client [TUITestClient](examples/monitoring_configuration_clients/src/main/java/TUITestClient.java):
    
    `java -jar examples/monitoring_configuration_clients/target/TUITestClient-exec.jar`
    
4.  Start the Multiple Server Client [TestMultipleServerClient](examples/test_implementations/src/main/java/client/TestMultipleServerClient.java):
    
    A server-initiated client that communicates with the three test servers with randomly repeated calls.
    
    `java -jar examples/test_implementations/target/TestMultipleServerClient-exec.jar`
    
5. Use the TUITestClient to do some example configuration, connection or monitoring:

    (TUITestClient is incomplete in terms of possible commands and properties of the features)
    Monitoring Example: Connect to the Client -> Execute 'trigger server discovery' -> Monitor messages
    
### License

Distributed under the MIT License. See `LICENSE` for more information.


### Contact

[SiLA Standard Slack](https://sila-standard.slack.com/join/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA) in the #sila\_edge\_gateway channel.

