package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class DeleteUploadedBinaryRequest {
    public static final String NAME = "DeleteUploadedBinaryRequest";
    public static final String TYPE = "<DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Length>36</Length> <Pattern>[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}</Pattern> </Constraints> </Constrained> </DataType>";
}
