package client.timer_impl;

import com.google.protobuf.ByteString;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.property.UnobservablePropertyImpl;

import java.util.function.Consumer;


@Slf4j
public class TimerValueProperty extends UnobservablePropertyImpl<SilaTimerServiceOuterClass.Get_Value_Parameters, SilaTimerServiceOuterClass.Get_Value_Responses> {

    private static final String PROPERTY_ID = "org.silastandard/examples/SilaTimerService/v1/Property/Value";

    @Builder
    public TimerValueProperty(SilaTimerServiceOuterClass.Get_Value_Parameters parameters, Consumer<SilaTimerServiceOuterClass.Get_Value_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, SilaTimerServiceOuterClass.Get_Value_Responses.parser(), errorCallback);
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.Get_Value_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError siLAError)  {
        log.info("Error received: {}", siLAError);
    }

    @Override
    public ByteString getPayload() {
        SilaTimerServiceOuterClass.Get_Value_Responses response = SilaTimerServiceOuterClass.Get_Value_Responses.newBuilder().build();
        return response.toByteString();
    }
}
