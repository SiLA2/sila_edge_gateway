package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import client.messages.utils.ParserUtils;

public class ObservablePropertyValueMessage extends ServerMessage{
    private final String propertyIdentifier;
    private final String propertyValue;

    public ObservablePropertyValueMessage(GatewayMonitoringServiceOuterClass.DataType_ObservablePropertyValueMessage message) {
        this.propertyIdentifier = message.getObservablePropertyValueMessage().getPropertyIdentifier().getValue();
        this.propertyValue = ParserUtils.getJsonFromAny(message.getObservablePropertyValueMessage().getPropertyValue());
    }

    @Override
    public String toString() {
        return "ObservablePropertyValueMessage{" +
                "\nPropertyIdentifier : " + propertyIdentifier +
                "\nPropertyValue : " + propertyValue +
                "\n}";
    }
}