package com.silastandard.gateway.client_side.message_services;

import com.google.protobuf.ByteString;
import com.google.protobuf.DynamicMessage;
import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import com.silastandard.gateway.server_side.models.SubscriptionStreamObserver;
import io.grpc.Context;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Handles Observable Properties
 */
@Slf4j
public class ObservablePropertyService extends MessageService {
    final Map<String, Context.CancellableContext> subscriptionContextMap = new HashMap<>();

    public ObservablePropertyService(@NonNull UUID silaServerUUID, @NonNull String clientName, @NonNull GatewayServices gatewayServices) {
        super(silaServerUUID, clientName, gatewayServices);
    }

    /**
     * Processes s Property subscription message.
     * It forwards the message to the CallService for gRPC-execution and
     * notifies the Monitoring Message Provider on the request and response messages.
     * A context in which the call is executed is created to be able to terminate the stream.
     * The termination of the stream is initiated by the CancelPropertySubscription message, for StreamComplete and StreamError.
     */
    public void subscribeProperty(SiLACloudConnector.PropertySubscription observablePropertyRequest,
                                  String requestUUID,
                                  StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        List<SiLACloudConnector.Metadata> metadataList = observablePropertyRequest.getMetadataList();
        String fullyQualifiedPropertyId = observablePropertyRequest.getFullyQualifiedPropertyId();
        ByteString emptyParameter = ByteString.EMPTY;
        log.info("Received Property subscription messages - fullyQualifiedPropertyId: {}", requestUUID);

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName, fullyQualifiedPropertyId, emptyParameter);

        try {
            gatewayServices.getMonitoringClientMessageProvider().onPropertySubscriptionMessage(siLAGatewayCall, fullyQualifiedPropertyId);
            Context.CancellableContext contextInfoSubscription = Context.current().withCancellation();

            gatewayServices.getServerManagerService().newCallService(siLAGatewayCall).executePropertySubscription(
                    metadataList,
                    contextInfoSubscription,
                    new SubscriptionStreamObserver.SubscriptionStreamCallback() {
                        @Override
                        public boolean onNext(DynamicMessage responseMessage) {

                            SiLACloudConnector.ObservablePropertyValue observablePropertyValue = SiLACloudConnector.ObservablePropertyValue.newBuilder()
                                    .setResult(responseMessage.toByteString())
                                    .build();

                            SiLACloudConnector.SILAServerMessage serverMessage = SiLACloudConnector.SILAServerMessage.newBuilder()
                                    .setRequestUUID(requestUUID)
                                    .setObservablePropertyValue(observablePropertyValue)
                                    .build();
                            serverMessageStream.onNext(serverMessage);
                            // monitoring: response
                            gatewayServices.getMonitoringServerMessageProvider().onObservablePropertyValueMessage(siLAGatewayCall, responseMessage);
                            return true;
                        }

                        @Override
                        public void onComplete() {
                            cancelPropertySubscription(requestUUID);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            if (throwable instanceof StatusRuntimeException) {
                                gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, (StatusRuntimeException) throwable);
                                SiLACloudConnector.SILAServerMessage serverErrorMessage = SiLACloudConnector.SILAServerMessage
                                        .newBuilder()
                                        .setPropertyError(SiLAErrors.retrieveSiLAError((StatusRuntimeException) throwable).orElse(null))
                                        .build();
                                serverMessageStream.onNext(serverErrorMessage);
                            }
                            cancelPropertySubscription(requestUUID);
                        }
                    }
            );
            subscriptionContextMap.put(requestUUID, contextInfoSubscription);
        } catch (StatusRuntimeException e) {
            gatewayServices.getMonitoringServerMessageProvider().onErrorMessage(siLAGatewayCall, e);
            serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null)).build());
        }
    }

    /**
     * Processes a Cancel Property Subscription message.
     * Cancels the stored context for the subscription stream to the UUID of the request.
     */
    public void cancelPropertySubscription(String requestUUID) {
        log.info("Cancel Property Subscription - Request UUID: {}", requestUUID);
        if(subscriptionContextMap.containsKey(requestUUID)) {
            subscriptionContextMap.get(requestUUID).cancel(null);
            subscriptionContextMap.remove(requestUUID);
            log.info("Subscription Stream with UUID " + requestUUID + " got cancelled");
        }
    }
}
