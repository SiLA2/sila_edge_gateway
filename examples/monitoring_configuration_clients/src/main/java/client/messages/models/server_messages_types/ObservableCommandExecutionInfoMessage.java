package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class ObservableCommandExecutionInfoMessage extends ServerMessage {
    private final String commandExecutionUUID;
    private final GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage.ObservableCommandExecutionInfoMessage_Struct.ExecutionInfo_Struct executionInfo;

    public ObservableCommandExecutionInfoMessage(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandExecutionInfoMessage message) {
        this.commandExecutionUUID = message.getObservableCommandExecutionInfoMessage().getCommandExecutionUUID().getValue();
        this.executionInfo = message.getObservableCommandExecutionInfoMessage().getExecutionInfo();
    }

    @Override
    public String toString() {
        return "ObservableCommandExecutionInfoMessage{" +
                "\nCommandExecutionUUID : " + commandExecutionUUID +
                "\nExecutionInfo : " + executionInfo +
                "\n}";
    }
}
