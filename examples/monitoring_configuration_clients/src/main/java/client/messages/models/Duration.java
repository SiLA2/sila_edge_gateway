package client.messages.models;

import lombok.Builder;

@Builder
public class Duration {
    private final Integer seconds;
    private final Integer nanos;
}
