package client.messages.utils;

import com.google.gson.JsonParser;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static sila_java.library.core.asynchronous.MethodPoller.await;

/**
 * Server Information of Test servers
 */
@Slf4j
@Getter
public final class ServerInfo {
    private final File configFile;
    private final int port;

    public ServerInfo(
            @NonNull final Path configPath,
            final int port
    ) {
        this.configFile = configPath.toFile();
        this.port = port;
        this.configFile.deleteOnExit();
    }

    public void clear() {
        this.configFile.delete();
    }

    public UUID getUUID(int timeout) throws IOException {
        try {
            await()
                    .atMost(Duration.ofSeconds(timeout))
                    .until(configFile::exists);

            return await()
                    .atMost(Duration.ofSeconds(timeout))
                    .untilPresent(()->getUUIDFromFile(configFile));
        } catch (ExecutionException | TimeoutException e) {
            throw new IOException("Configuration file wasn't created: " + this.configFile.toString());
        }
    }

    /**
     * Helper to wait until content is written
     */
    private static Optional<UUID> getUUIDFromFile(File configFile) {
        try {
            final String configurationContent = FileUtils.readFileToString(
                    configFile,
                    Charset.defaultCharset()
            );
            final String uuid = new JsonParser().parse(configurationContent)
                    .getAsJsonObject()
                    .getAsJsonPrimitive("uuid")
                    .getAsString();

            log.info("UUID retrieved: " + uuid);
            return Optional.of(UUID.fromString(uuid));
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
