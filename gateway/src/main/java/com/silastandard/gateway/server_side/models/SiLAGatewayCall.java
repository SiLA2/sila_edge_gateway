package com.silastandard.gateway.server_side.models;

import com.google.protobuf.ByteString;
import com.silastandard.gateway.server_side.server_management.ServerManagerService;
import com.silastandard.gateway.utils.FeatureIdConverter;
import lombok.Getter;
import lombok.NonNull;

import java.util.UUID;

/**
 * Represents a generic SiLA Gateway Call from a SilA client.
 */
@Getter
public final class SiLAGatewayCall {
    private final UUID identifier = UUID.randomUUID();
    private final UUID serverUUID;
    private final String clientNameId;
    private final String fullyQualifiedFeatureId;
    private final String fullyQualifiedCallId;
    private final String callId ;
    private final ByteString payload;

    /**
     * Call Constructor
     * @param serverUUID As added to the {@link ServerManagerService}
     * @param clientNameId The client name as client ID
     * @param fullyQualifiedCallId the fullyQualifiedCommandId or fullyQualifiedPropertyId
     * @param payload Payload
     *
     * @see <a href="https://developers.google.com/protocol-buffers/docs/proto3#json">Protobuf Spec</a>
     */
    public SiLAGatewayCall(
            @NonNull final UUID serverUUID,
            @NonNull final String clientNameId,
            @NonNull final String fullyQualifiedCallId,
            @NonNull final ByteString payload
    ) {
        this.serverUUID = serverUUID;
        this.clientNameId = clientNameId;
        this.fullyQualifiedCallId = fullyQualifiedCallId;
        this.fullyQualifiedFeatureId = FeatureIdConverter.getFullyQualifiedFeatureID(fullyQualifiedCallId);
        this.callId = FeatureIdConverter.getCallID(fullyQualifiedCallId);
        this.payload = payload;
    }

    public SiLAGatewayCall(
            @NonNull final UUID serverUUID,
            @NonNull final String clientNameId
    ) {
        this.serverUUID = serverUUID;
        this.clientNameId = clientNameId;
        this.fullyQualifiedCallId = "";
        this.fullyQualifiedFeatureId = "";
        this.callId = "";
        this.payload = ByteString.EMPTY;
    }
}
