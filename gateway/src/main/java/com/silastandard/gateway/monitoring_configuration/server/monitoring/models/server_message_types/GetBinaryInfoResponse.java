package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class GetBinaryInfoResponse {
    public static final String NAME = "GetBinaryInfoResponse";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>BinarySize</Identifier> <DisplayName>Binary Size</DisplayName> <Description>Storage space is allocated based on the Binary Size.</Description> <DataType> <Basic>Integer</Basic> </DataType> </Element> <Element> <Identifier>LifetimeOfBinary</Identifier> <DisplayName>Lifetime Of Binary</DisplayName> <Description>Duration during which a Binary Transfer UUID is valid.</Description> <DataType> <DataTypeIdentifier>Duration</DataTypeIdentifier> </DataType> </Element> </Structure> </DataType>";
}
