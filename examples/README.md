## Example and Test implementations
Current example and test implementations:\
(The executable jars can be launched from the root directory of the project)

---

### Example monitoring and configuration clients
The clients are used to test and demonstrate the monitoring and configuration functions of the gateway.

&nbsp;

#### - TextUserInterface Client
A text user interface client for testing the configuration and monitoring functionalities. It is not complete for all possible calls.
It tries to connect to a server-initiated Client with HOST = "localhost" and PORT = 50308 by default. Use [ConnectionConfigurationProvider](monitoring_configuration_clients/src/main/java/connection_configurattion/ConnectionConfigurationProvider.java) to change the default connection settings.

Start the TUITest client:
- `java -jar examples/monitoring_configuration_clients/target/TUITestClient-exec.jar`

#### - Configuration Client
A client to test the GatewayConfigurationService Feature.

Start the configuration client:
- `java -jar examples/monitoring_configuration_clients/target/ConfigurationClient-exec.jar`

#### - Monitoring Client
A client to test the GatewayMonitoringService Feature. This client just monitors the messages. 
To see messages, a server-initiated Client must communicate with SiLA servers, all of which are connected to the gateway.
The example [Multiple Server Client ](test_implementations/src/main/java/client/TestMultipleServerClient.java)  with the corresponding SiLA Servers can be used for this scenario.

Start the monitoring client:
- `java -jar examples/monitoring_configuration_clients/target/MonitoringClient-exec.jar`

---


### Test implementations to test the gateway
These test implementations contain three SiLA<1.0> servers and three different server-initiated clients for different testing purposes.
The executable jars can be launched from the root directory of the project.

&nbsp;

#### - Test servers
Three SiLA<1.0> servers to test the gateway with the following server-initiated clients.
- `java -jar examples/test_implementations/target/CoverControllerServer-exec.jar -n local`
- `java -jar examples/test_implementations/target/HelloSiLAServer-exec.jar -n local`
- `java -jar examples/test_implementations/target/TimerServer-exec.jar -n local`

#### - Multiple Server Client
A server-initiated client that communicates with the three test servers with randomly repeated calls.

Start the MultipleServerClient:
- `java -jar examples/test_implementations/target/TestMultipleServerClient-exec.jar`

#### - Log server information
This is an implementation for a server-initiated client which uses the SiLA Service.
The goal is to show that the gateway can handle the used message types specified in the SiLA Service.

Start the log server information client:
- `java -jar examples/test_implementations/target/LogServerInformationClient-exec.jar`

#### - Load Test Client
A server-initiated client that issues repeated calls to the gateway after opening the streams for the test servers.
It records how many RPCs have been completed after a certain time.

Start the LoadTestClient:
- `java -jar examples/test_implementations/target/LoadTestClient-exec.jar`
