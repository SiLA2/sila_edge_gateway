package client.messages;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class ServerResponseMessage {
    private final String fromSiLAServer;
    private final String toSiLAClient;
    private final ServerResponseMessageTypes message;


    public ServerResponseMessage(@NonNull String fromSiLAServer, @NonNull String toSiLAClient, @NonNull ServerResponseMessageTypes message) {
        this.fromSiLAServer = fromSiLAServer;
        this.toSiLAClient = toSiLAClient;
        this.message = message;
    }

    public interface ServerResponseMessageTypes {
        String toJson();
    }

    @Override
    public String toString() {
        return String.format("From SiLA Server: %s\nTo SiLA Client: %s\nMessage:%s\n",
                fromSiLAServer,
                toSiLAClient,
                message);
    }
}





