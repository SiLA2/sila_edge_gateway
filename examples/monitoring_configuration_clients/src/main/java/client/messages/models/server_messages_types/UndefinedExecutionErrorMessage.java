package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class UndefinedExecutionErrorMessage extends ServerMessage {
    private final String errorMessage;

    public UndefinedExecutionErrorMessage(GatewayMonitoringServiceOuterClass.DataType_UndefinedExecutionErrorMessage message) {
        this.errorMessage = message.getUndefinedExecutionErrorMessage().getValue();
    }

    @Override
    public String toString() {
        return "UndefinedExecutionErrorMessage{" +
                "\nError Message : " + errorMessage +
                "\n}";
    }
}