package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class CommandExecutionInfoSubscriptionMessage extends ClientMessage {
    private final String commandIdentifier;
    private final String commandExecutionUUID;

    public CommandExecutionInfoSubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_CommandExecutionInfoSubscriptionMessage message) {
        this.commandIdentifier = message.getCommandExecutionInfoSubscriptionMessage().getCommandIdentifier().getValue();
        this.commandExecutionUUID = message.getCommandExecutionInfoSubscriptionMessage().getCommandExecutionUUID().getValue();
    }

    @Override
    public String toString() {
        return "CommandExecutionInfoSubscriptionMessage{" +
                "\nCommandIdentifier : " + commandIdentifier +
                "\nCommandExecutionUUID : " +  commandExecutionUUID +
                "\n}";
    }
}
