package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.server_message_types;

public class ObservableCommandResponse {
    public static final String NAME = "ObservableCommandResponse";
    public static final String TYPE = "<DataType> <Structure> <Element> <Identifier>CommandExecutionUUID</Identifier> <DisplayName>Command Execution UUID</DisplayName> <Description>The unique identifier of the command.</Description> <DataType> <Constrained> <DataType> <Basic>String</Basic> </DataType> <Constraints> <Length>36</Length> <Pattern>[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}</Pattern> </Constraints> </Constrained> </DataType> </Element> <Element> <Identifier>Result</Identifier> <DisplayName>Result</DisplayName> <Description> The result of the Observable Command Response. Any Type corresponds to the original Observable Command DataType described in the feature. </Description> <DataType> <Basic>Any</Basic> </DataType> </Element> </Structure> </DataType>";
}
