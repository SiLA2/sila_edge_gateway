package client.covercontroller_impl;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.instruments.covercontroller.v1.CoverControllerOuterClass;
import sila_java.library.cloud.client.command.UnobservableCommandImpl;

import java.util.function.Consumer;

@Slf4j
public class OpenCoverCommand extends UnobservableCommandImpl<CoverControllerOuterClass.CloseCover_Parameters, CoverControllerOuterClass.CloseCover_Responses> {
    private static final String COMMAND_ID = "org.silastandard/instruments/CoverController/v1/Command/OpenCover";

    @Builder
    public OpenCoverCommand(CoverControllerOuterClass.CloseCover_Parameters parameters, Consumer<CoverControllerOuterClass.CloseCover_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(COMMAND_ID, parameters, resultCallback, errorCallback, CoverControllerOuterClass.CloseCover_Responses.parser());
    }

    @Override
    public void onResponse(CoverControllerOuterClass.CloseCover_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError silaError) {
        log.warn("error received: {}", silaError);
    }
}