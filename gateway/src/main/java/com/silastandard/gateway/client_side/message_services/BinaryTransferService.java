package com.silastandard.gateway.client_side.message_services;

import com.silastandard.gateway.GatewayServices;
import com.silastandard.gateway.server_side.call_services.GatewayBinaryCallService;
import com.silastandard.gateway.server_side.models.SiLAGatewayCall;
import io.grpc.Context;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.jodah.expiringmap.ExpirationListener;
import net.jodah.expiringmap.ExpiringMap;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.server_base.binary_transfer.errors.BinaryTransferErrorHandler;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Handles Binary Transfer messages
 */
@Slf4j
public class BinaryTransferService extends MessageService{
    private final ExpiringMap<UUID, StreamResources> uploadChunkResourceMap = ExpiringMap.builder().asyncExpirationListener(uploadChunkExpirationListener()).variableExpiration().build();
    private final ExpiringMap<UUID, StreamResources> getChunkResourceMap = ExpiringMap.builder().asyncExpirationListener(uploadChunkExpirationListener()).variableExpiration().build();
    private final static long contextLifetimeDuration = 30; // sec

    public BinaryTransferService(@NonNull UUID silaServerUUID, @NonNull String clientName, @NonNull GatewayServices gatewayServices) {
        super(silaServerUUID, clientName, gatewayServices);
    }

    private ExpirationListener<UUID, StreamResources> uploadChunkExpirationListener(){
        return (uuid, streamResources) -> streamResources.delete();
    }

    /**
     * A Class to hold the resources for uploadChunk and getChunk Streams.
     */
    @Getter
    public class StreamResources {
        private final Context.CancellableContext context;
        private final StreamObserver streamObserver;

        public StreamResources(Context.CancellableContext context, StreamObserver streamObserver) {
            this.context = context;
            this.streamObserver = streamObserver;
        }

        public void delete() {
            context.cancel(null);
        }
    }


    /**
     * Processes a Create Binary message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified about the request and response message as well as Errors.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeCreateBinaryUploadRequest(SiLACloudConnector.CreateBinaryUploadRequest createBinaryUploadRequest) {
        SiLABinaryTransfer.CreateBinaryRequest createBinaryRequest = createBinaryUploadRequest.getCreateBinaryRequest();
        List<SiLACloudConnector.Metadata> metadataList = createBinaryUploadRequest.getMetadataList();

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName);

        try {
            final GatewayBinaryCallService binaryCallService = gatewayServices.getServerManagerService().newBinaryCallService(silaServerUUID);

            gatewayServices.getMonitoringClientMessageProvider().onCreateBinaryUploadRequest(siLAGatewayCall, createBinaryRequest);
            SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse = binaryCallService.createBinaryRequest(createBinaryRequest, metadataList);
            gatewayServices.getMonitoringServerMessageProvider().onCreateBinaryResponse(siLAGatewayCall, createBinaryResponse);

            return serverMessageBuilder.setCreateBinaryResponse(createBinaryResponse);
        } catch (StatusRuntimeException e) {
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                return serverMessageBuilder.setBinaryTransferError(binaryTransferError.get());
            }
            return null;
        }
    }

    /**
     * Processes a Delete Uploaded Binary message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified about the request and response message as well as Errors.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeDeleteUploadedBinaryRequest(SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName);
        final GatewayBinaryCallService binaryCallService = gatewayServices.getServerManagerService().newBinaryCallService(silaServerUUID);

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        try {
            UUID binaryTransferUUID;
            try {
                binaryTransferUUID = UUID.fromString(deleteBinaryRequest.getBinaryTransferUUID());
            } catch (IllegalArgumentException e) {
                return SiLACloudConnector.SILAServerMessage.newBuilder()
                        .setBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.newBuilder()
                                .setErrorType(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID)
                                .setMessage("BinaryTransferUUID not a valid UUID")
                                .build());
            }

            gatewayServices.getMonitoringClientMessageProvider().onDeleteUploadedBinaryRequest(siLAGatewayCall, deleteBinaryRequest);
            SiLABinaryTransfer.DeleteBinaryResponse deleteBinaryResponse = binaryCallService.deleteUploadedBinaryUpload(deleteBinaryRequest);
            gatewayServices.getMonitoringServerMessageProvider().onDeleteBinaryResponse(siLAGatewayCall);

            deleteResourcesAndRemoveUUID(uploadChunkResourceMap, binaryTransferUUID);
            deleteResourcesAndRemoveUUID(getChunkResourceMap, binaryTransferUUID);

            return serverMessageBuilder.setDeleteBinaryResponse(deleteBinaryResponse);
        } catch (StatusRuntimeException e) {
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                return serverMessageBuilder.setBinaryTransferError(binaryTransferError.get());
            }
            return null;
        }
    }

    /**
     * Processes a Delete Binary message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified about the request and response message as well as Errors.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeDeleteDownloadedBinaryRequest(SiLABinaryTransfer.DeleteBinaryRequest deleteBinaryRequest) {
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName);
        final GatewayBinaryCallService binaryCallService = gatewayServices.getServerManagerService().newBinaryCallService(silaServerUUID);

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        try {
            UUID binaryTransferUUID;
            try {
                binaryTransferUUID = UUID.fromString(deleteBinaryRequest.getBinaryTransferUUID());
            } catch (IllegalArgumentException e) {
                return SiLACloudConnector.SILAServerMessage.newBuilder()
                        .setBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.newBuilder()
                                .setErrorType(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID)
                                .setMessage("BinaryTransferUUID not a valid UUID")
                                .build());
            }

            gatewayServices.getMonitoringClientMessageProvider().onDeleteDownloadedBinaryRequest(siLAGatewayCall, deleteBinaryRequest);
            SiLABinaryTransfer.DeleteBinaryResponse deleteBinaryResponse = binaryCallService.deleteDownloadedBinaryUpload(deleteBinaryRequest);
            gatewayServices.getMonitoringServerMessageProvider().onDeleteBinaryResponse(siLAGatewayCall);

            deleteResourcesAndRemoveUUID(uploadChunkResourceMap, binaryTransferUUID);
            deleteResourcesAndRemoveUUID(getChunkResourceMap, binaryTransferUUID);

            return serverMessageBuilder.setDeleteBinaryResponse(deleteBinaryResponse);
        } catch (StatusRuntimeException e) {
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                return serverMessageBuilder.setBinaryTransferError(binaryTransferError.get());
            }
            return null;
        }
    }

    /**
     * Processes a UploadChunkRequest message.
     * It looks if the binaryTransferUUID has already been used recently(under 30s) for upload and gets the streamObserver for sending the messages.
     * If this is not the case, new stream resources are saved (starts the UploadChunk RPC). The message gets send over the new stream.
     * A Streams expires if no new request is sent 30 seconds after the last request.
     * The Monitoring Message Provider is notified about the request and response message as well as Errors.
     */
    public void executeUploadChunkRequest(SiLACloudConnector.SILAClientMessage clientMessage,
                                          StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        SiLABinaryTransfer.UploadChunkRequest uploadChunkRequest = clientMessage.getUploadChunkRequest();
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName);
        Optional<UUID> optionalBinaryTransferUUID = isValidUUID(uploadChunkRequest.getBinaryTransferUUID(), serverMessageStream);
        try {
            if(optionalBinaryTransferUUID.isPresent()) {
                UUID binaryTransferUUID = optionalBinaryTransferUUID.get();

                StreamResources uploadChunkResource = uploadChunkResourceMap.get(binaryTransferUUID);
                if(uploadChunkResource == null) {
                    Context.CancellableContext context = Context.current().withCancellation();
                    context.run(() -> {
                        StreamObserver<SiLABinaryTransfer.UploadChunkRequest> streamObserver = uploadChunk(serverMessageStream, binaryTransferUUID, siLAGatewayCall);
                        StreamResources streamResources = new StreamResources(context, streamObserver);
                        uploadChunkResourceMap.put(binaryTransferUUID, streamResources);
                        streamObserver.onNext(uploadChunkRequest);
                    });
                } else {
                    uploadChunkResourceMap.setExpiration(binaryTransferUUID, contextLifetimeDuration, TimeUnit.SECONDS);
                    uploadChunkResource.streamObserver.onNext(uploadChunkRequest);
                }
                gatewayServices.getMonitoringClientMessageProvider().onUploadChunkRequest(siLAGatewayCall, uploadChunkRequest);
            }
        } catch (StatusRuntimeException e) {
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder().setBinaryTransferError(binaryTransferError.get()).build());
            }
        }
    }

    private StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunk(StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream, UUID binaryTransferUUID, SiLAGatewayCall siLAGatewayCall) {
        final GatewayBinaryCallService binaryCallService = gatewayServices.getServerManagerService().newBinaryCallService(silaServerUUID);
        StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunkRequestStreamObserver = binaryCallService.uploadChunk(new StreamObserver<SiLABinaryTransfer.UploadChunkResponse>() {
                @Override
                public void onNext(SiLABinaryTransfer.UploadChunkResponse uploadChunkResponse) {
                    gatewayServices.getMonitoringServerMessageProvider().onUploadChunkResponse(siLAGatewayCall, uploadChunkResponse);
                    serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder()
                            .setUploadChunkResponse(uploadChunkResponse)
                            .build());
                }

                @Override
                public void onError(Throwable throwable) {
                    sendThrowableError(siLAGatewayCall, throwable, serverMessageStream);
                    deleteResourcesAndRemoveUUID(uploadChunkResourceMap, binaryTransferUUID);
                }

                @Override
                public void onCompleted() {
                    deleteResourcesAndRemoveUUID(uploadChunkResourceMap, binaryTransferUUID);
                }
        });
        return uploadChunkRequestStreamObserver;
    }

    /**
     * Processes a GetBinaryInfo message.
     * It forwards the request message to the CallService for gRPC-execution and waits for the result.
     * The Monitoring Message Provider is notified about the request and response message as well as Errors.
     */
    public SiLACloudConnector.SILAServerMessage.Builder executeGetBinaryInfoRequest(SiLACloudConnector.SILAClientMessage clientMessage) {
        SiLABinaryTransfer.GetBinaryInfoRequest getBinaryInfoRequest = clientMessage.getGetBinaryInfoRequest();

        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName);
        final GatewayBinaryCallService binaryCallService = gatewayServices.getServerManagerService().newBinaryCallService(silaServerUUID);

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        try {

            gatewayServices.getMonitoringClientMessageProvider().onGetBinaryInfoRequest(siLAGatewayCall, getBinaryInfoRequest);
            SiLABinaryTransfer.GetBinaryInfoResponse getBinaryInfoResponse = binaryCallService.getBinaryInfo(getBinaryInfoRequest);
            gatewayServices.getMonitoringServerMessageProvider().onGetBinaryInfoResponse(siLAGatewayCall, getBinaryInfoResponse);

            return serverMessageBuilder.setGetBinaryResponse(getBinaryInfoResponse);
        } catch (StatusRuntimeException e) {
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                return serverMessageBuilder.setBinaryTransferError(binaryTransferError.get());
            }
            return null;
        }
    }

    /**
     * Processes a GetChunkRequest message.
     * It looks if the binaryTransferUUID has already been used recently(under 30s) for upload and gets the streamObserver for sending the messages.
     * If this is not the case, new stream resources are saved (starts the UploadChunk RPC). The message gets send over the new stream.
     * A Streams expires if no new request is sent 30 seconds after the last request.
     * The Monitoring Message Provider is notified about the request and response message as well as Errors.
     */
    public void executeGetChunkRequest(SiLACloudConnector.SILAClientMessage clientMessage,
                                StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        SiLABinaryTransfer.GetChunkRequest getChunkRequest = clientMessage.getGetChunkRequest();
        SiLAGatewayCall siLAGatewayCall = new SiLAGatewayCall(silaServerUUID, clientName);
        Optional<UUID> optionalBinaryTransferUUID = isValidUUID(getChunkRequest.getBinaryTransferUUID(), serverMessageStream);
        try {
            if(optionalBinaryTransferUUID.isPresent()) {
                UUID binaryTransferUUID = optionalBinaryTransferUUID.get();

                StreamResources getChunkResource = uploadChunkResourceMap.get(binaryTransferUUID);
                if(getChunkResource == null) {
                    Context.CancellableContext context = Context.current().withCancellation();
                    context.run(() -> {
                        StreamObserver<SiLABinaryTransfer.GetChunkRequest> streamObserver = getChunk(serverMessageStream, binaryTransferUUID, siLAGatewayCall);
                        StreamResources streamResources = new StreamResources(context, streamObserver);
                        uploadChunkResourceMap.put(binaryTransferUUID, streamResources);
                        streamObserver.onNext(getChunkRequest);
                    });
                } else {
                    uploadChunkResourceMap.setExpiration(binaryTransferUUID, contextLifetimeDuration, TimeUnit.SECONDS);
                    getChunkResource.streamObserver.onNext(getChunkRequest);
                }

                gatewayServices.getMonitoringClientMessageProvider().onGetChunkRequest(siLAGatewayCall, getChunkRequest);
            }
        } catch (StatusRuntimeException e) {
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder().setBinaryTransferError(binaryTransferError.get()).build());
            }
        }
    }

    private StreamObserver<SiLABinaryTransfer.GetChunkRequest> getChunk(StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream, UUID binaryTransferUUID, SiLAGatewayCall siLAGatewayCall) {
        final GatewayBinaryCallService binaryCallService = gatewayServices.getServerManagerService().newBinaryCallService(silaServerUUID);

        StreamObserver<SiLABinaryTransfer.GetChunkRequest> getChunkRequestStreamObserver = binaryCallService.getChunk(new StreamObserver<SiLABinaryTransfer.GetChunkResponse>() {
            @Override
            public void onNext(SiLABinaryTransfer.GetChunkResponse getChunkResponse) {
                gatewayServices.getMonitoringServerMessageProvider().onGetChunkResponse(siLAGatewayCall, getChunkResponse);
                serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder()
                        .setGetChunkResponse(getChunkResponse)
                        .build());
            }

            @Override
            public void onError(Throwable throwable) {
                sendThrowableError(siLAGatewayCall, throwable, serverMessageStream);
                deleteResourcesAndRemoveUUID(getChunkResourceMap, binaryTransferUUID);
            }

            @Override
            public void onCompleted() {
                deleteResourcesAndRemoveUUID(getChunkResourceMap, binaryTransferUUID);
            }
        });
        return getChunkRequestStreamObserver;
    }

    private void sendThrowableError(SiLAGatewayCall siLAGatewayCall, Throwable throwable, StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        try {
            StatusRuntimeException e = (StatusRuntimeException) throwable;
            Optional<SiLABinaryTransfer.BinaryTransferError> binaryTransferError = BinaryTransferErrorHandler.retrieveBinaryTransferError(e);
            if(binaryTransferError.isPresent()) {
                gatewayServices.getMonitoringServerMessageProvider().onBinaryTransferError(siLAGatewayCall, binaryTransferError.get());
                serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder().setBinaryTransferError(binaryTransferError.get()).build());
            }
        } catch (Exception e) {
            log.error("Exception couldn't be transformed into BinaryTransferError", e);
        }
    }

    private Optional<UUID> isValidUUID(String binaryTransferUUID, StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        try {
            return Optional.of(UUID.fromString(binaryTransferUUID));
        } catch (IllegalArgumentException e) {
            serverMessageStream.onNext(SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.newBuilder()
                            .setErrorType(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID)
                            .setMessage("BinaryTransferUUID not a valid UUID")
                            .build())
                    .build());
            return Optional.empty();
        }
    }

    private void deleteResourcesAndRemoveUUID(ExpiringMap<UUID, StreamResources> resMap, UUID binaryTransferUUID) {
        StreamResources streamResources = resMap.get(binaryTransferUUID);
        if(streamResources != null) {
            streamResources.delete();
            resMap.remove(binaryTransferUUID);
        }
    }
}