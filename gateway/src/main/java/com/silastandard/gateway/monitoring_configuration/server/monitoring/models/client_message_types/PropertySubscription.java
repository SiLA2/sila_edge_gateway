package com.silastandard.gateway.monitoring_configuration.server.monitoring.models.client_message_types;

public class PropertySubscription {
    public static final String NAME = "PropertySubscription";
    public static final String TYPE = "<DataType><Constrained><DataType><Basic>String</Basic></DataType><Constraints><FullyQualifiedIdentifier>PropertyIdentifier</FullyQualifiedIdentifier></Constraints></Constrained></DataType>";
}