package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class ValidationErrorMessage extends ServerMessage {
    private final String parameter;
    private final String errorMessage;

    public ValidationErrorMessage(GatewayMonitoringServiceOuterClass.DataType_ValidationErrorMessage message) {
        this.parameter = message.getValidationErrorMessage().getParameter().getValue();
        this.errorMessage = message.getValidationErrorMessage().getMessage().getValue();
    }

    @Override
    public String toString() {
        return "ValidationErrorMessage{" +
                "\nParameter : " + parameter +
                "\nError Message : " + errorMessage +
                "\n}";
    }
}