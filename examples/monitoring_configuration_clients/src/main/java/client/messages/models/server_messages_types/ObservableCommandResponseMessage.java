package client.messages.models.server_messages_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;
import client.messages.utils.ParserUtils;

import java.util.Map;
import java.util.stream.Collectors;

public class ObservableCommandResponseMessage extends ServerMessage {
    private final String commandExecutionUUID;
    private final Map<String, Object> response;

    public ObservableCommandResponseMessage(GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage message) {
        GatewayMonitoringServiceOuterClass.DataType_ObservableCommandResponseMessage.ObservableCommandResponseMessage_Struct payload= message.getObservableCommandResponseMessage();
        this.commandExecutionUUID = message.getObservableCommandResponseMessage().getCommandExecutionUUID().getValue();
        response = payload.getResultList().stream().collect(Collectors.toMap(r -> r.getIdentifier().getValue(), r-> ParserUtils.getJsonFromAny(r.getValue())));
    }

    @Override
    public String toString() {
        return "ObservableCommandResponseMessage{" +
                "\nCommandExecutionUUID : " + commandExecutionUUID +
                "\nResponse:\n" + response +
                "\n}";
    }
}
