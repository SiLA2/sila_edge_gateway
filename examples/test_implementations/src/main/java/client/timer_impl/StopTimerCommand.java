package client.timer_impl;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.command.UnobservableCommandImpl;

import java.util.function.Consumer;

@Slf4j
@Getter
public class StopTimerCommand extends UnobservableCommandImpl<SilaTimerServiceOuterClass.StopTimer_Parameters, SilaTimerServiceOuterClass.StopTimer_Responses> {

    private static final String COMMAND_ID = "org.silastandard/examples/SilaTimerService/v1/Command/StopTimer";

    @Builder
    public StopTimerCommand(SilaTimerServiceOuterClass.StopTimer_Parameters parameters, Consumer<SilaTimerServiceOuterClass.StopTimer_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(COMMAND_ID, parameters, resultCallback, errorCallback, SilaTimerServiceOuterClass.StopTimer_Responses.parser());
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.StopTimer_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.info("error received: {}", error);
    }
}
