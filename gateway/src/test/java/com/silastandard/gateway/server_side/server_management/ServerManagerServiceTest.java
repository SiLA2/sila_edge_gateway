package com.silastandard.gateway.server_side.server_management;

import junit.framework.Assert;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila_java.examples.test_server.TestServer;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelperBuilder;
import client.messages.utils.ServerFinderGateway;
import client.messages.utils.ServerInfo;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static client.messages.utils.ScanUtilsForTest.scanAndWaitServerState;
import static client.messages.utils.ScanUtilsForTest.waitServerState;


@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServerManagerServiceTest {
    private static final int TIMEOUT = 60; // [s]
    private static final String HOST = "127.0.0.1";
    private static final String NETWORK_INTERFACE = "local";
    private final ServerManagerService serverManager = new ServerManagerService();

    private final ServerInfo serverInfo_1 = new ServerInfo(Paths.get("./config/testserver1.cfg"), 50100);
    private final ServerInfo serverInfo_2 = new ServerInfo(Paths.get("./config/testserver2.cfg"), 50101);
    private final ServerInfo serverInfo_3 = new ServerInfo(Paths.get("./config/testserver3.cfg"), 50102);

    private final List<ServerInfo> SERVER_INFORMATION = Arrays.asList(
            serverInfo_1,
            serverInfo_2,
            serverInfo_3
    );

    @AfterAll
    void cleanup() {
        serverManager.close();
    }

    @BeforeEach
    void checkManager() {
        // No server online
        Assert.assertTrue(serverManager.getServers().isEmpty());
    }

    @AfterEach
    void cleanManager() {
        serverManager.clear();

        SERVER_INFORMATION.forEach(
                ServerInfo::clear
        );
    }

    @Test
    void testManagerDiscovery() throws IOException, InterruptedException, TimeoutException, ExecutionException {
        // Start server 1 in parallel
        log.info("Starting first Test Server");

        TestServer testServer1 =  startTestServer(serverInfo_1, "TestServer1", NETWORK_INTERFACE);
        UUID serverUUID1 = serverInfo_1.getUUID(TIMEOUT);

        // Two Servers online
        log.info("Starting second Test Server and discover it");

        TestServer testServer2 = startTestServer(serverInfo_2, "TestServer2", NETWORK_INTERFACE);
        UUID serverUUID2 = serverInfo_2.getUUID(TIMEOUT);
        scanAndWaitServerState(serverManager, serverUUID2, Server.Status.ONLINE, TIMEOUT);
        stopTestServer(testServer2);

        // One server online, one offline
        serverUUID2 = serverInfo_2.getUUID(TIMEOUT);
        waitServerState(serverManager, serverUUID2, Server.Status.OFFLINE, TIMEOUT);

        final ServerFinderGateway server2Finder = ServerFinderGateway.filterBy(serverManager, ServerFinderGateway.Filter.uuid(serverUUID2));
        final Server managerServer2 = server2Finder.findOne().orElseThrow(AssertionError::new);
        log.info("Second Test Server discovered and offline detected");

        // make Sure Server 1 is up and detected online
        waitServerState(serverManager, serverUUID1, Server.Status.ONLINE, TIMEOUT);

        Assert.assertEquals(1, ServerFinderGateway.filterBy(serverManager, ServerFinderGateway.Filter.status(Server.Status.ONLINE),ServerFinderGateway.Filter.type("Test Server")).find().size());

        // Offline server up again

        testServer2 = startTestServer(serverInfo_2, "TestServer2", NETWORK_INTERFACE);
        serverUUID2 = serverInfo_2.getUUID(TIMEOUT);
        waitServerState(serverManager, serverUUID2, Server.Status.ONLINE, TIMEOUT);

        server2Finder.findOne().ifPresent(
                server -> Assert.assertEquals(
                        managerServer2.getConfiguration().getUuid(),
                        server.getConfiguration().getUuid()
                )
        );

        log.info("Second Test Server up again with same UUID");
        stopTestServer(testServer2);

        waitServerState(serverManager, managerServer2.getConfiguration().getUuid(), Server.Status.OFFLINE, TIMEOUT);
        Assert.assertEquals(Server.Status.OFFLINE, managerServer2.getStatus());

        // Two servers online, one offline (same port)

        try (final TestServer server3 = new TestServer(
                ArgumentHelperBuilder.build( serverInfo_2.getPort(), "TestServer2",serverInfo_3.getConfigFile().getPath(), NETWORK_INTERFACE)
        )) {
            UUID serverUUID3  = serverInfo_3.getUUID(TIMEOUT);

            scanAndWaitServerState(serverManager, serverUUID3, Server.Status.ONLINE, TIMEOUT);

            // Test server online, not equal and stop old server
            Assertions.assertNotEquals(serverUUID2, serverUUID3);
            stopTestServer(testServer1);

            waitServerState(serverManager, serverInfo_1.getUUID(TIMEOUT), Server.Status.OFFLINE, TIMEOUT);
        }
    }

    @Test
    void testServerOnDifferentPorts() throws IOException, InterruptedException, TimeoutException, ExecutionException {

/*
        TestServer testServer1 =  startTestServer(serverInfo_1, "TestServer1", NETWORK_INTERFACE);
        UUID serverUUID1 = serverInfo_1.getUUID(TIMEOUT);

        waitServerState(serverUUID1, Server.Status.ONLINE, TIMEOUT);


        stopTestServer(testServer1);
        waitServerState(serverUUID1, Server.Status.OFFLINE, TIMEOUT);

        // Restart Server 1 on different port and check if uuid found

        testServer1 =  startTestServer(new ServerInfo(Paths.get("./config/testserver1.cfg"), 50104), "TestServer1", NETWORK_INTERFACE);
        serverUUID1 = serverInfo_1.getUUID(TIMEOUT);


        scanAndWaitServerState(serverUUID1, Server.Status.ONLINE, TIMEOUT);
        stopTestServer(testServer1);
        waitServerState(serverUUID1, Server.Status.OFFLINE, TIMEOUT);
        1
 */


    }


    private TestServer startTestServer(ServerInfo serverInfo, String serverName, String netInterface) {
        return new TestServer(
                ArgumentHelperBuilder.build(serverInfo.getPort(), serverName,serverInfo.getConfigFile().getPath(), netInterface));
    }

    private void stopTestServer(TestServer testServer) {
        testServer.close();
    }

}
