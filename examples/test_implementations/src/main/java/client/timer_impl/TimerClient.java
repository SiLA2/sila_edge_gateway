package client.timer_impl;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.core.sila.types.SiLAString;

import java.util.function.Consumer;

@Slf4j
public class TimerClient {
    private final CloudConnectedServer server;

    public TimerClient(CloudConnectedServer server) {
        this.server = server;
    }

    public void startTimer(String parameterName, Consumer<String> resultCallback) {
        StartTimerCommand startTimerCommand = StartTimerCommand.builder()
                .parameters(SilaTimerServiceOuterClass.StartTimer_Parameters.newBuilder()
                        .setName(SiLAString.from(parameterName))
                        .build())
                .resultCallback(r -> resultCallback.accept(r.getTimerValue().toString()))
                .build();
        server.getCommandExecutionService().executeCommand(startTimerCommand);
    }

    public void resetTimer(Consumer<String> resultCallback) {
        ResetTimerCommand resetTimerCommand = ResetTimerCommand.builder()
                .parameters(SilaTimerServiceOuterClass.ResetTimer_Parameters.getDefaultInstance())
                .resultCallback(r -> resultCallback.accept(r.getTimerValue().toString())).build();
        server.getCommandExecutionService().executeCommand(resetTimerCommand);
    }

    public void stopTimer(Consumer<String> resultCallback) {
        StopTimerCommand stopTimerCommand = StopTimerCommand.builder()
                .parameters(SilaTimerServiceOuterClass.StopTimer_Parameters.getDefaultInstance())
                .resultCallback(r -> resultCallback.accept(r.getTimerValue().toString())).build();
        server.getCommandExecutionService().executeCommand(stopTimerCommand);
    }

    public void startTime(Consumer<String> resultCallback) {
        StartTimeProperty startTimeProperty = StartTimeProperty.builder()
                .parameters(SilaTimerServiceOuterClass.Get_StartTime_Parameters.getDefaultInstance())
                .resultCallback(r -> resultCallback.accept(r.getStartTime().getValue()))
                .build();
        server.getUnobservablePropertyService().readProperty(startTimeProperty);
    }

    public void getTimerValue(Consumer<String> resultCallback) {
        TimerValueProperty timerValueProperty = TimerValueProperty.builder()
                .resultCallback(responses -> resultCallback.accept(responses.getValue().toString())).build();
        server.getUnobservablePropertyService().readProperty(timerValueProperty);
    }
}
