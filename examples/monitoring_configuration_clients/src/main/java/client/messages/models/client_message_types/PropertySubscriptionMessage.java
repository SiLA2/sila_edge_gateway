package client.messages.models.client_message_types;

import sila2.org.gateway.gateway.gatewaymonitoringservice.v1.GatewayMonitoringServiceOuterClass;

public class PropertySubscriptionMessage extends ClientMessage {
    private final String propertyIdentifier;

    public PropertySubscriptionMessage(GatewayMonitoringServiceOuterClass.DataType_PropertySubscriptionMessage message) {
        this.propertyIdentifier = message.getPropertySubscriptionMessage().getValue();
    }

    @Override
    public String toString() {
        return "PropertySubscriptionMessage{" +
                "\nPropertyIdentifier : " + propertyIdentifier +
                "\n}";
    }
}